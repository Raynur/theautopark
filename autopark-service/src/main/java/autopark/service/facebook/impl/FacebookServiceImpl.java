package autopark.service.facebook.impl;

import autopark.dao.IRoleDAO;
import autopark.dao.IRoleUserDAO;
import autopark.dao.IUserDAO;
import autopark.domain.Role;
import autopark.domain.RoleUser;
import autopark.domain.User;
import autopark.dto.PrincipalUser;
import autopark.http.IHttpRepository;
import autopark.service.AutoparkServiceException;
import autopark.service.IUserService;
import autopark.service.facebook.IFacebookService;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;

/*
FACEBOOK
Max Park       aupark!123
https://developers.facebook.com/apps/591918377643932/dashboard/

app id= 591918377643932
secret= a6ddaaa3ca6b9c3f692f02f961cc60f0

https://developers.facebook.com/docs/facebook-login/manually-build-a-login-flow#login описание api
 */
public class FacebookServiceImpl implements IFacebookService {
    private final static Logger log = LoggerFactory.getLogger(FacebookServiceImpl.class);
    private final static String START_URL = "http://www.facebook.com/dialog/oauth?client_id=";
    private final static String RED_URI = "&redirect_uri=";
    private final static String CLIENT_SECRET = "&client_secret=";


    private final static String CALLBACK_START_URL = "http://localhost:8080/";
    private final static String UTF8 = "UTF-8";
    private final static String LOGIN_URL = "oauth/fblogin";

    private final static String GET_TOKEN_START_URL = "https://graph.facebook.com/oauth/access_token?client_id=";
    private final static String GET_ME_START_URL = "https://graph.facebook.com/me?";

    private final static String ACCESS_TOKEN = "access_token";

    private static ObjectMapper jacksonMapper;
    static {
        jacksonMapper = new ObjectMapper();
        jacksonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    @Autowired
    private IUserDAO userDAO;


    @Autowired
    @Qualifier("encoder")
    private PasswordEncoder passwordEncoder;

    @Autowired
    @Qualifier("myUserService")
    private IUserService userService;

    @Autowired
    private IRoleUserDAO roleUserDAO;

    @Autowired
    private IRoleDAO roleDAO;


    private String appId;
    private String secret;


    @Autowired
    private IHttpRepository httpRepository;


    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }



    public String getFBLoginUrl() {
        final StringBuilder sb = new StringBuilder();
        sb.append(START_URL);
        sb.append(appId);
        sb.append(RED_URI);

        try {
            sb.append(URLEncoder.encode(CALLBACK_START_URL + LOGIN_URL, UTF8));
        } catch (final UnsupportedEncodingException e) {
            e.printStackTrace();
            log.error("url is wrong", e);
        }

        sb.append("&scope=email");

        return sb.toString();
    }

    public String getToken(String code) throws AutoparkServiceException {
        try {
            final StringBuilder sb = new StringBuilder();
            sb.append(GET_TOKEN_START_URL);
            sb.append(appId);
            sb.append(RED_URI);
            try {
                sb.append(URLEncoder.encode(CALLBACK_START_URL + LOGIN_URL, UTF8));
            } catch (final UnsupportedEncodingException e) {
                e.printStackTrace();
                log.error("url is wrong", e);
            }

            sb.append(CLIENT_SECRET);
            sb.append(secret);
            sb.append("&code=");
            sb.append(code);


            String res = httpRepository.callHttpGet(sb.toString());
            if (res.startsWith("{")) {
                throw new AutoparkServiceException("error on requesting token: " + res + " with code: " + code);
            }
            //return extractToken(res);
            return res;
        } catch (final Exception e) {
            log.error("Failed to complete authentication", e);
        }
        return null;

    }

    public FBInfoDTO getMe(String token) throws AutoparkServiceException {
        try {
            final StringBuilder sb = new StringBuilder();
            sb.append(GET_ME_START_URL);
            sb.append(token);

            String json = httpRepository.callHttpGet(sb.toString());
            return extractMe(json);
        } catch (final Exception e) {
            log.error("Failed to get me ", e);
            throw new AutoparkServiceException("error on requesting token: " + token);
        }
    }


    private static String extractToken(String res){
        String[] pars = res.split("&");
        for(String s: pars){
            if(s.startsWith(ACCESS_TOKEN)){
                return s.substring(ACCESS_TOKEN.length()+1);
            }
        }
        return null;
    }

    private static FBInfoDTO extractMe(String json){
        try {
            return jacksonMapper.readValue(json, FBInfoDTO.class);
        } catch (IOException e) {
            log.error("Cannot parse Json into class, " + e);
        } catch (Exception e) {
            log.error("Incorrect or empty response from sharepoint,  " + e);
        }
        return null;
    }


    public void processLogin(String code) throws AutoparkServiceException{
        String token = null;
        try {
            token = getToken(code);
        } catch (AutoparkServiceException e) {
            log.error("Failed to get token");
            throw new AutoparkServiceException("error on requesting token: " + token);
        }

        FBInfoDTO info;
        try {
            info = getMe(token);
        } catch (AutoparkServiceException e) {
            log.error("Failed to get token");
            //todo something
            throw new AutoparkServiceException("error on requesting info: " + token);
        }

        login(info);

    }

    private void login(FBInfoDTO infoDTO){
        User user;
        if(StringUtils.isNotEmpty(infoDTO.getEmail())){
            user = userDAO.getByEmail(infoDTO.getEmail());
        }else{
            user = userDAO.getByFacebookId(infoDTO.getId());
        }

        if(user == null){
            user = createFbUser(infoDTO);
        }

        PrincipalUser principalUser = userService.assemblePrincipalUser(user,true);

        Authentication auth = new UsernamePasswordAuthenticationToken(principalUser, null, principalUser.getAuthorities());

        SecurityContextHolder.getContext().setAuthentication(auth);
    }



    private User createFbUser(FBInfoDTO infoDTO){
        User user = new User();
        user.setEmail(infoDTO.getEmail());
        user.setFacebookId(infoDTO.getId());
        user.setCreationDate(new Date());
        if(infoDTO.getName()!=null){
            user.setName(infoDTO.getName());
        }else{
            StringBuilder theName = new StringBuilder();
            if(StringUtils.isNotEmpty(infoDTO.getFirst_name())){
                theName.append(infoDTO.getFirst_name().trim());
            }
            if(StringUtils.isNotEmpty(infoDTO.getLast_name())){
                theName.append(" ");
                theName.append(infoDTO.getLast_name().trim());
            }
            user.setName(theName.toString().trim());
        }

        user.setPassword(passwordEncoder.encode(infoDTO.getId()));

        Long userId = userDAO.save(user);

        RoleUser roleUser = new RoleUser();
        roleUser.setUSER_ID(userId);
        roleUser.setROLE_ID(Role.ROLE_USER_ID);
        roleUserDAO.save(roleUser);

        user.addRole(roleDAO.getUserRole());
        return user;
    }

}