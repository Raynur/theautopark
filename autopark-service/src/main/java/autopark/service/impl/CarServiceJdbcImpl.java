package autopark.service.impl;

import autopark.dao.ICarDAO;
import autopark.dao.IUserDAO;
import autopark.domain.User;
import autopark.dto.CarDTO;
import autopark.dto.CarSearchDTO;
import autopark.dto.DemandDTO;
import autopark.dto.PrincipalUser;
import autopark.service.ICarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by  01 on 15.02.2016.
 */
@Service("carServiceJdbc")
public class CarServiceJdbcImpl implements ICarService{

    @Autowired
    private ICarDAO carDAO;

    @Autowired
    private IUserDAO userDAO;

    public List<CarDTO> getCars() {
        return carDAO.getCarsJdbc();
    }

    public CarDTO getCar(Long id) {
        return null;
    }

    @Override
    public List<CarDTO> searchCars(CarSearchDTO dto) {
        return carDAO.searchCars(dto);
    }

    public List<CarDTO> getMyCars() {
        PrincipalUser principalUser = (PrincipalUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principalUser == null){
            return null;
        }

        User user = userDAO.getByEmail(principalUser.getUsername());

        if(user == null){
            return null;
        }
        List<CarDTO> res = carDAO.getUserCars(user.getId());


        return res;
    }
}
