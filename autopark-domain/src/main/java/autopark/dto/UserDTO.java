package autopark.dto;


import java.util.Date;
// Interest link about validation
// http://www.journaldev.com/2668/spring-mvc-form-validation-example-using-annotation-and-custom-validator-implementation
public class UserDTO {
    private Long id;

    private String name;

    private String email;

    private String phone;
    private String password;
    private String passwordRetry;
    private Date birthday;
    private Date creationDate;
    private String remoteAddress;
    private boolean isActivated;
    private ImageFileDTO imageFileDTO;
    private Long myPhotoImageFileId;



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordRetry() {
        return passwordRetry;
    }

    public void setPasswordRetry(String passwordRetry) {
        this.passwordRetry = passwordRetry;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "name='" + name + '\'' +
                ",phone='" + phone + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", birthday='" + birthday + '\'' +
                ", creationDate='" + creationDate + '\'' +
                "}";
    }

    @Override
    public boolean equals(Object obj) {
        if(this.id == null){
            return true;
        }
        return this.id.equals(((UserDTO)obj).getId());
    }

    @Override
    public int hashCode() {
        return id.intValue();
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public void setRemoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
    }

    public boolean isActivated() {
        return isActivated;
    }

    public void setActivated(boolean activated) {
        isActivated = activated;
    }

    public ImageFileDTO getImageFileDTO() {
        return imageFileDTO;
    }

    public void setImageFileDTO(ImageFileDTO imageFileDTO) {
        this.imageFileDTO = imageFileDTO;
    }

    public Long getMyPhotoImageFileId() {
        return myPhotoImageFileId;
    }

    public void setMyPhotoImageFileId(Long myPhotoImageFileId) {
        this.myPhotoImageFileId = myPhotoImageFileId;
    }
}
