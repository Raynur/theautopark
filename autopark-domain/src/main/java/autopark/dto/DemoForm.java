package autopark.dto;

import autopark.dto.RecaptchaForm;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * 02.04.2016.
 */
public class DemoForm extends RecaptchaForm{

    @NotEmpty
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
