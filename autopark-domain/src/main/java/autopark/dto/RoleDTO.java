package autopark.dto;

import org.springframework.security.core.GrantedAuthority;

/**
 * Created by aro on 05.03.2016.
 */
public class RoleDTO  implements GrantedAuthority {
    private String authority;

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

}
