package autopark.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ap_role_user")
public class RoleUser extends Root {
    private Long USER_ID;
    private Long ROLE_ID;

    public Long getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(Long USER_ID) {
        this.USER_ID = USER_ID;
    }

    public Long getROLE_ID() {
        return ROLE_ID;
    }

    public void setROLE_ID(Long ROLE_ID) {
        this.ROLE_ID = ROLE_ID;
    }
}
