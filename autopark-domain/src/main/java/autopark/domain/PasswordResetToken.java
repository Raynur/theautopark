package autopark.domain;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * 19.03.2016.
 */
@Entity
@Table(name = "ap_password_reset_token")
public class PasswordResetToken extends Root{

    /*@Id*/
    private String email;

    @Column(name = "token_value")
    private String tokenValue;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "token_expiration_date")
    private Date expirationDate;

    public String getTokenValue() {
        return tokenValue;
    }

    public void setTokenValue(final String tokenValue) {
        this.tokenValue = tokenValue;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDate() {
        return expirationDate;
    }

    public void setDate(Date tokenDate) {
        this.expirationDate = tokenDate;
    }

}