package autopark.domain;

/**
 * Created by  01 on 15.02.2016.
 */
public enum PriceUnitEnum {
    PER_HOUR,
    PER_KM,
    MIN
}
