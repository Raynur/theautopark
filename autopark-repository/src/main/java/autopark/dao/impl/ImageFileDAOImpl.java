package autopark.dao.impl;

import autopark.dao.ImageFileDAO;
import autopark.domain.ImageFile;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by aro on 09.04.2016.
 */
@Repository
public class ImageFileDAOImpl extends RootDAOImpl<ImageFile> implements ImageFileDAO {
    private static final Log log = LogFactory.getLog(ImageFileDAOImpl.class);

    @Autowired
    public ImageFileDAOImpl(SessionFactory sessionFactory) {
        super("autopark.domain.ImageFile", ImageFile.class);
    }

}
