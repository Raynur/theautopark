package autopark.dao.impl;

import autopark.dao.GenericDAO;
import autopark.domain.Locus;

/**
 * 10.04.2016.
 */
public interface ILocusDAO extends GenericDAO<Long, Locus> {
}
