package autopark.web.controller.tracker;

import autopark.dao.impl.ILocusDAO;
import autopark.domain.Locus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;

/**
 * 10.04.2016.
 */
@Controller
public class RestTracker { //TODO name

    @Autowired
    private ILocusDAO locusDAO;

    @RequestMapping(value = "/locus", method = RequestMethod.POST)
    public ResponseEntity<String> createLocus(
            @RequestParam(value = "lat", required = false) double latitude,
            @RequestParam(value = "lon", required = false) double longitude
            /*@RequestParam(value = "uid", required = false) double uid*/) {

        Locus locus = new Locus();
        locus.setLat(latitude);
        locus.setLon(longitude);
        locus.setDate(new Date());
        //TODO locus.setUid(uid);

        if(locusDAO.add(locus)){
            return new ResponseEntity<String>(HttpStatus.OK);
        }else {
            return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
