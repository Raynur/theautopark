package autopark.web.controller.dic;

import autopark.domain.CarLoadingCapacityEnum;
import autopark.domain.CarTypeEnum;
import autopark.domain.CarVendorEnum;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class DicEnumController {

    @RequestMapping(value = "/dic/vendors")
    @ResponseBody
    public CarVendorEnum[] getVendors(){
        return CarVendorEnum.values();
    }

    @RequestMapping(value = "/dic/types")
    @ResponseBody
    public CarTypeEnum[] getTypes(){
        return CarTypeEnum.values();
    }

    @RequestMapping(value = "/dic/capacity")
    @ResponseBody
    public CarLoadingCapacityEnum[] getCap(){
        return CarLoadingCapacityEnum.values();
    }

}



