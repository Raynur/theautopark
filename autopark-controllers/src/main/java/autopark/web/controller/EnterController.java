package autopark.web.controller;

import autopark.AutoparkRepositoryException;
import autopark.service.IRecaptchaService;
import autopark.service.IWebService;
import autopark.service.RecaptchaServiceException;
import autopark.service.facebook.IFacebookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
public class EnterController {
    @Autowired
    private IFacebookService facebookService;


    @Autowired
    private IRecaptchaService recaptchaService;

    @Autowired
    private IWebService webService;

/*
    private RecaptchaFormValidator recaptchaFormValidator;

    @Autowired
    public EnterController(RecaptchaFormValidator recaptchaFormValidator) {

        this.recaptchaFormValidator = recaptchaFormValidator;
    }
*/

    @RequestMapping(value = "/demo", method = RequestMethod.GET)
    public String demoGet() {
        return "WEB-INF/content/demo.jsp";
        ///WEB-INF/content/enter.jsp"
    }

    @RequestMapping(value = "/demo", method = RequestMethod.POST)
    public String demoPost(/*@ModelAttribute("form") @Valid DemoForm form, BindingResult result,*/ ModelMap modelMap, HttpServletRequest request) {
        String gRecaptchaResponse = request.getParameter("g-recaptcha-response");
        String remoteAddress = null;
        try {
            remoteAddress = webService.getMyIp();
        } catch (AutoparkRepositoryException e) {
            e.printStackTrace();
        }

        try {
            if (recaptchaService.isResponseValid(remoteAddress, gRecaptchaResponse)) {
                return "redirect:/main";
            }
        } catch (RecaptchaServiceException e) {
            e.printStackTrace();
        }

        return "WEB-INF/content/demo.jsp";
    }


    @RequestMapping(value = "/enter")
    public String handle(@RequestParam(required = false) Integer error, ModelMap modelMap) {
        modelMap.put("error",error);
        modelMap.put("fburl",facebookService.getFBLoginUrl());
        return Constants.ENTER;
    }



}



