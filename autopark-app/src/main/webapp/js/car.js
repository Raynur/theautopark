$(document).ready(function() {
    var dialog,form;
    var description = $("#ac-description");
    var name        = $("#ac-name");

    function addComment() {
        $.ajax({
            url     : '/comment/add',
            dataType: 'json',
            data    : "description="+$("#ac-description").val()+"&commentatorName="+$("#ac-name").val()+"&entityId="+$("#ac-id").val(),
            async   : false,
            error   : function (error) {
                //console.log('loadAreas', error);
                dialog.dialog( "close" );
            },
            success : function (data) {
                dialog.dialog( "close" );

            }
        });

    }

    dialog = $( "#dialog-form" ).dialog({
        autoOpen: false,
        height: 500,
        width: 500,
        modal: true,
        buttons: {
            "Создать": addComment,
            "Отмена": function() {
                dialog.dialog( "close" );
            }
        },
        close: function() {
            form[ 0 ].reset();
        },
        open: function( event, ui ) {
            $(this).closest('form').empty();
            //hack for icon image
            $(this).closest(".ui-dialog")
                .find(".ui-dialog-titlebar-close")
                .removeClass("ui-dialog-titlebar-close")
                .html("<span class='ui-button-icon-primary ui-icon ui-icon-closethick'></span>");
            $(this).closest(".ui-dialog").find(".ui-dialog-titlebar").find("button").attr("style","float:right");
            //hack for icon image

        }

    });

    form = dialog.find( "form" ).on( "submit", function( event ) {
        event.preventDefault();
        addComment();
    });
    $( ".addcom" ).button().on( "click", function() {
        dialog.dialog( "open" );
    });



});

